/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/



import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'
import { GroundCover } from './modules/GroundCover'
import {BuilderHUD} from './modules/BuilderHUD'


const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})


scene.addComponent(transform)
engine.addEntity(scene)

const grassyFineTexture = new Texture("materials/Tileable-Textures/grassy-512-1-0.png")
const grassyFineMaterial = new Material()
grassyFineMaterial.albedoTexture = grassyFineTexture

const ground = new GroundCover (0,0,16, 0.01,16, grassyFineMaterial,false)
ground.setParent(scene)



const box = spawnBoxX(3.6,1.1,7.38,  0,0,0,  3.2,3.01,3.04)

const blueMaterial = new Material()
blueMaterial.albedoColor = Color3.Blue()
blueMaterial.metallic = .3
blueMaterial.roughness = 0.0
blueMaterial.reflectivityColor
box.addComponent(blueMaterial)

const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 3.6,0.992,10.604,  0,0,0,  1.59,1.614,1.72)

const naturePack = spawnGltfX(new GLTFShape("models/NaturePack/Nature-Pack-gltf-v1.3.gltf"), 8,0,13, 0,180,0,  0.5,0.5,0.5)

const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 4,0,3, 0,180,0,  0.1,0.1,0.1)
const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 14,1.5,8, 0,0,0, 0.05, 0.05, 0.05)


const point1 = new Vector3(4,4,3)
const point2 = new Vector3(8,6,13)
const point3 = new Vector3(3.6, 2.8 ,7.38)
const point4 = new Vector3(8, 4, 8)
const path: Vector3[] = [point1, point2, point3, point4]

const TURN_TIME = 0.9

@Component('lerpData')
export class LerpData {
  array: Vector3[] = path
  origin: number = 0
  target: number = 1
  fraction: number = 0
}

@Component("timeOut")
export class TimeOut {
  timeLeft: number
  constructor( time: number){
    this.timeLeft = time
  }
}

export const paused = engine.getComponentGroup(TimeOut)


export class BirdFly {
  update(dt: number) {
    let transform = bird.getComponent(Transform)
    let path = bird.getComponent(LerpData)
    path.fraction += dt / 3
    if (path.fraction < 1) {
      transform.position = Vector3.Lerp(
        path.array[path.origin],
        path.array[path.target],
        path.fraction
      )
    } else {
      path.origin = path.target
      path.target += 1
      if (path.target >= path.array.length) {
        path.target = 0
      }
      path.fraction = 0
      transform.lookAt(path.array[path.target])
    }
  }
}

engine.addSystem(new BirdFly())

const flyspeed = 4
const animator = new Animator();
let clipFly = new AnimationState("fly")
//clipWalk.looping = true
clipFly.setParams({speed: flyspeed, weight: 0.5})
animator.addClip(clipFly);
bird.addComponent(animator);


bird.addComponent(new LerpData())

clipFly.play()

const hud:BuilderHUD =  new BuilderHUD()
hud.setDefaultParent(scene)
hud.attachToEntity(box)
hud.attachToEntity(cube)


